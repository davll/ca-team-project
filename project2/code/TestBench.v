`include "Defs.v"

`define CYCLE_TIME 50			

module TestBench;

reg				Clk;
reg				Reset;
reg				Start;
integer			i, outfile, outfile2, counter;
reg					flag;
reg		[26:0]		address;
reg		[23:0]		tag;
reg		[4:0]		index;

wire	[256-1:0]	mem_CPU_data; 
wire				mem_CPU_ack; 	
wire	[256-1:0]	CPU_mem_data; 
wire	[32-1:0]	CPU_mem_addr; 	
wire				CPU_mem_enable; 
wire				CPU_mem_write; 

always #(`CYCLE_TIME/2) Clk = ~Clk;	

CPU cpu(
	.clock_i  (Clk),
    .reset_i  (!Reset),
	.start_i  (Start),
	
	.mem_data_i(mem_CPU_data), 
	.mem_ack_i(mem_CPU_ack), 	
	.mem_data_o(CPU_mem_data), 
	.mem_addr_o(CPU_mem_addr), 	
	.mem_enable_o(CPU_mem_enable), 
	.mem_write_o(CPU_mem_write)
);

Data_Memory Data_Memory
(
	.clk_i(Clk),
    .rst_i  (Reset),
	.addr_i(CPU_mem_addr),
	.data_i(CPU_mem_data),
	.enable_i(CPU_mem_enable),
	.write_i(CPU_mem_write),
	.ack_o(mem_CPU_ack),
	.data_o(mem_CPU_data)
);
  
initial begin
	counter = 1;
	
	// initialize instruction memory (2KB)
	for(i=0; i<cpu.IMem.SIZE; i=i+1) begin
		cpu.IMem._content[i] = 32'b0;
	end
	
	// initialize data memory	(16KB)
`ifdef USE_CACHE
	for(i=0; i<512; i=i+1) begin
		Data_Memory.memory[i] = 256'b0;
	end
`else
	for (i=0; i<cpu.DMem.SIZE; i=i+1)
    begin
      cpu.DMem._content[i] = 0;
    end
`endif
		
	// initialize cache memory	(1KB)
`ifdef USE_CACHE
	for(i=0; i<32; i=i+1) begin
		cpu.dcache.dcache_tag_sram.memory[i] = 24'b0;
		cpu.dcache.dcache_data_sram.memory[i] = 256'b0;
	end
`else
`endif
	
	// initialize Register File
	for(i=0; i<cpu.RegFile.SIZE; i=i+1) begin
		cpu.RegFile._content[i] = 32'b0;
	end
	
	// Load instructions into instruction memory
	$readmemb("instruction.txt", cpu.IMem._content);
	
	// Open output file
	outfile = $fopen("output.txt") | 1;
	outfile2 = $fopen("cache.txt") | 1;
	
	
	// Set Input n into data memory at 0x00
`ifdef USE_CACHE
	Data_Memory.memory[0] = 256'h5;		// n = 5 for example
`else
	cpu.DMem._content[0] = 8'h5;
`endif
	
    Clk = 0;
    Reset = 0;
    Start = 0;
    
    #(`CYCLE_TIME/4) 
    Reset = 1;
    Start = 1;

    
end
  
always@(posedge Clk) begin
`ifdef USE_CACHE
	if(counter == 150) begin	// store cache to memory
		$fdisplay(outfile, "Flush Cache! \n");
		for(i=0; i<32; i=i+1) begin
			tag = cpu.dcache.dcache_tag_sram.memory[i];
			index = i;
			address = {tag[21:0], index};
			Data_Memory.memory[address] = cpu.dcache.dcache_data_sram.memory[i];
		end 
	end
`endif
	if(counter > 150) begin	// stop 
		$finish;
	end
		
	$fdisplay(outfile, "cycle = %d, Start = %b", counter, Start);
	// print PC 
	$fdisplay(outfile, "PC = %d", cpu.PC.pc_o);
	
	// print Registers
	$fdisplay(outfile, "Registers");
    $fdisplay(outfile, "R0(r0) = %h, R8 (t0) = %h, R16(s0) = %h, R24(t8) = %h", 
              cpu.RegFile._content[0], cpu.RegFile._content[8] , 
              cpu.RegFile._content[16], cpu.RegFile._content[24]);
    $fdisplay(outfile, "R1(at) = %h, R9 (t1) = %h, R17(s1) = %h, R25(t9) = %h", 
              cpu.RegFile._content[1], cpu.RegFile._content[9] , 
              cpu.RegFile._content[17], cpu.RegFile._content[25]);
    $fdisplay(outfile, "R2(v0) = %h, R10(t2) = %h, R18(s2) = %h, R26(k0) = %h", 
              cpu.RegFile._content[2], cpu.RegFile._content[10], 
              cpu.RegFile._content[18], cpu.RegFile._content[26]);
    $fdisplay(outfile, "R3(v1) = %h, R11(t3) = %h, R19(s3) = %h, R27(k1) = %h", 
              cpu.RegFile._content[3], cpu.RegFile._content[11], 
              cpu.RegFile._content[19], cpu.RegFile._content[27]);
    $fdisplay(outfile, "R4(a0) = %h, R12(t4) = %h, R20(s4) = %h, R28(gp) = %h", 
              cpu.RegFile._content[4], cpu.RegFile._content[12], 
              cpu.RegFile._content[20], cpu.RegFile._content[28]);
    $fdisplay(outfile, "R5(a1) = %h, R13(t5) = %h, R21(s5) = %h, R29(sp) = %h", 
              cpu.RegFile._content[5], cpu.RegFile._content[13], 
              cpu.RegFile._content[21], cpu.RegFile._content[29]);
    $fdisplay(outfile, "R6(a2) = %h, R14(t6) = %h, R22(s6) = %h, R30(s8) = %h", 
              cpu.RegFile._content[6], cpu.RegFile._content[14], 
              cpu.RegFile._content[22], cpu.RegFile._content[30]);
    $fdisplay(outfile, "R7(a3) = %h, R15(t7) = %h, R23(s7) = %h, R31(ra) = %h", 
              cpu.RegFile._content[7], cpu.RegFile._content[15], 
              cpu.RegFile._content[23], cpu.RegFile._content[31]);

	// print Data Memory
`ifdef USE_CACHE
	$fdisplay(outfile, "Data Memory: 0x0000 = %h", Data_Memory.memory[0]);
	$fdisplay(outfile, "Data Memory: 0x0020 = %h", Data_Memory.memory[1]);
	$fdisplay(outfile, "Data Memory: 0x0040 = %h", Data_Memory.memory[2]);
	$fdisplay(outfile, "Data Memory: 0x0060 = %h", Data_Memory.memory[3]);
	$fdisplay(outfile, "Data Memory: 0x0080 = %h", Data_Memory.memory[4]);
	$fdisplay(outfile, "Data Memory: 0x00A0 = %h", Data_Memory.memory[5]);
	$fdisplay(outfile, "Data Memory: 0x00C0 = %h", Data_Memory.memory[6]);
	$fdisplay(outfile, "Data Memory: 0x00E0 = %h", Data_Memory.memory[7]);
	$fdisplay(outfile, "Data Memory: 0x0400 = %h", Data_Memory.memory[32]);
`else
    for(i = 0; i < 8 || i == 32; i=i+1)
    begin
      $fdisplay(outfile, "Data Memory: 0x%h = %h",  ( i * 16'd32 ), 
                                                   {cpu.DMem._content[32*i+31] , 
                                                    cpu.DMem._content[32*i+30] , 
                                                    cpu.DMem._content[32*i+29] , 
                                                    cpu.DMem._content[32*i+28] , 
                                                    cpu.DMem._content[32*i+27] , 
                                                    cpu.DMem._content[32*i+26] , 
                                                    cpu.DMem._content[32*i+25] , 
                                                    cpu.DMem._content[32*i+24] , 
                                                    cpu.DMem._content[32*i+23] , 
                                                    cpu.DMem._content[32*i+22] , 
                                                    cpu.DMem._content[32*i+21] , 
                                                    cpu.DMem._content[32*i+20] , 
                                                    cpu.DMem._content[32*i+19] , 
                                                    cpu.DMem._content[32*i+18] , 
                                                    cpu.DMem._content[32*i+17] , 
                                                    cpu.DMem._content[32*i+16] , 
                                                    cpu.DMem._content[32*i+15] , 
                                                    cpu.DMem._content[32*i+14] , 
                                                    cpu.DMem._content[32*i+13] , 
                                                    cpu.DMem._content[32*i+12] , 
                                                    cpu.DMem._content[32*i+11] , 
                                                    cpu.DMem._content[32*i+10] , 
                                                    cpu.DMem._content[32*i+ 9] , 
                                                    cpu.DMem._content[32*i+ 8] , 
                                                    cpu.DMem._content[32*i+ 7] , 
                                                    cpu.DMem._content[32*i+ 6] , 
                                                    cpu.DMem._content[32*i+ 5] , 
                                                    cpu.DMem._content[32*i+ 4] , 
                                                    cpu.DMem._content[32*i+ 3] , 
                                                    cpu.DMem._content[32*i+ 2] , 
                                                    cpu.DMem._content[32*i+ 1] , 
                                                    cpu.DMem._content[32*i+ 0]
                                                   });
      if (i == 7)
        i = 31;
    end
`endif
	
	if (1) // Debug
	begin
	  $display("MEM_stall = %d", cpu.MEM_stall);
	  $display("dcache.mem_ack_i = %d", cpu.dcache.mem_ack_i);
	  $display("dcache.hit = %d", cpu.dcache.hit);
	  $display("dcache.r_hit_data = %h", cpu.dcache.r_hit_data);
	  $display("dmem.mem_ack_o = %d", Data_Memory.ack_o);
	end
	
	$fdisplay(outfile, "\n");
	
	// print Data Cache Status
`ifdef USE_CACHE
	if(cpu.dcache.p1_stall_o && cpu.dcache.state==0) begin
		if(cpu.dcache.sram_dirty) begin
			if(cpu.dcache.p1_MemWrite_i) 
				$fdisplay(outfile2, "Cycle: %d, Write Miss, Address: %h, Write Data: %h (Write Back!)", counter, cpu.dcache.p1_addr_i, cpu.dcache.p1_data_i);
			else if(cpu.dcache.p1_MemRead_i) 
				$fdisplay(outfile2, "Cycle: %d, Read Miss , Address: %h, Read Data : %h (Write Back!)", counter, cpu.dcache.p1_addr_i, cpu.dcache.p1_data_o);
		end
		else begin
			if(cpu.dcache.p1_MemWrite_i) 
				$fdisplay(outfile2, "Cycle: %d, Write Miss, Address: %h, Write Data: %h", counter, cpu.dcache.p1_addr_i, cpu.dcache.p1_data_i);
			else if(cpu.dcache.p1_MemRead_i) 
				$fdisplay(outfile2, "Cycle: %d, Read Miss , Address: %h, Read Data : %h", counter, cpu.dcache.p1_addr_i, cpu.dcache.p1_data_o);
		end
		flag = 1'b1;
	end
	else if(!cpu.dcache.p1_stall_o) begin
		if(!flag) begin
			if(cpu.dcache.p1_MemWrite_i) 
				$fdisplay(outfile2, "Cycle: %d, Write Hit , Address: %h, Write Data: %h", counter, cpu.dcache.p1_addr_i, cpu.dcache.p1_data_i);
			else if(cpu.dcache.p1_MemRead_i) 
				$fdisplay(outfile2, "Cycle: %d, Read Hit  , Address: %h, Read Data : %h", counter, cpu.dcache.p1_addr_i, cpu.dcache.p1_data_o);
		end
		flag = 1'b0;
	end
`endif
	
	counter = counter + 1;
end
  
endmodule
