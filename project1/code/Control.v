`include "Defs.v"

module Control (
  opcode_i, 
  
  ctrl_EX_o, 
  ctrl_M_o,
  ctrl_WB_o,
  ctrl_J_o
);
  
  //
  localparam Opcode_RType = 6'b000000;
  localparam Opcode_ADDI =  6'b001000;
  localparam Opcode_SW =    6'b101011;
  localparam Opcode_LW =    6'b100011;
  localparam Opcode_J =     6'b000010;
  localparam Opcode_BEQ =   6'b000100;
  
  // Input (from instruction[31:26])
  input [5:0] opcode_i;
  
  // EX Vector
  localparam Ctrl_EX_Bits = 4;
  // ALUSrcB as [0]; 0: RT, 1: Immediate
  // ALUOp as [2:1];
  // RegDst as [3]; 1: RD, 0: RT
  
  output [Ctrl_EX_Bits-1:0] ctrl_EX_o;
  reg [Ctrl_EX_Bits-1:0] ctrl_EX_o;
  
  always @ ( opcode_i )
  begin
    case (opcode_i)
      Opcode_RType: ctrl_EX_o = { 1'b1, `ALUOP_FUNCT, 1'b0 };
      Opcode_ADDI:  ctrl_EX_o = { 1'b0, `ALUOP_ADD, 1'b1 };
      Opcode_SW:    ctrl_EX_o = { 1'b0, `ALUOP_ADD, 1'b1 };
      Opcode_LW:    ctrl_EX_o = { 1'b0, `ALUOP_ADD, 1'b1 };
      default: ctrl_EX_o = 0;
    endcase
  end
  
  // M Vector
  localparam Ctrl_M_Bits = 2;
  // MemRead as [0];
  // MemWrite as [1];
  
  output [Ctrl_M_Bits-1:0] ctrl_M_o;
  reg [Ctrl_M_Bits-1:0] ctrl_M_o;
  
  always @ ( opcode_i )
  begin
    case (opcode_i)
      Opcode_SW: ctrl_M_o = 2'b10;
      Opcode_LW: ctrl_M_o = 2'b01;
      default: ctrl_M_o = 0;
    endcase
  end
  
  // WB Vector
  localparam Ctrl_WB_Bits = 2;
  // RegWrite as [0]; 
  // Mem2Reg as [1]; 0: ALU result, 1: Memory
  
  output [Ctrl_WB_Bits-1:0] ctrl_WB_o;
  reg [Ctrl_WB_Bits-1:0] ctrl_WB_o;
  
  always @ ( opcode_i )
  begin
    case (opcode_i)
      Opcode_RType: ctrl_WB_o = 2'b01;
      Opcode_ADDI:  ctrl_WB_o = 2'b01;
      Opcode_LW:    ctrl_WB_o = 2'b11;
      default: ctrl_WB_o = 0;
    endcase
  end
  
  // J Vector
  localparam Ctrl_J_Bits = 2;
  // Jump as [0]; 0: other, 1: jump
  // Branch as [1]; 0: other, 1: branch
  // other = default: (pc+4)
  
  output [Ctrl_J_Bits-1:0] ctrl_J_o;
  reg [Ctrl_J_Bits-1:0] ctrl_J_o;
  
  always @ ( opcode_i )
  begin
    case (opcode_i)
      Opcode_J:   ctrl_J_o = 2'b01;
      Opcode_BEQ: ctrl_J_o = 2'b10;
      default: ctrl_J_o = 0;
    endcase
  end
  
endmodule
