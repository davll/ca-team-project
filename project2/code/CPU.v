`include "Defs.v"

module CPU ( clock_i, reset_i, start_i,
  mem_data_i,
  mem_ack_i,
  mem_data_o,
  mem_addr_o,
  mem_enable_o,
  mem_write_o
 );
  
  input clock_i, reset_i, start_i;
  
  // to Data Memory interface
  input	[256-1:0]	mem_data_i; 
  input				mem_ack_i; 	
  output	[256-1:0]	mem_data_o; 
  output	[32-1:0]	mem_addr_o; 	
  output				mem_enable_o; 
  output				mem_write_o; 
  
  
  //
  wire pc_run, control_propagation, pc_flush;
  wire [31:0] pc_next;
  
  //
  wire [4:0] rd_addr;
  wire [31:0] rd_data;
  wire rd_write;
  
  //
  wire [31:0] EX_forwarded_rs_data, EX_forwarded_rt_data;
  
  //
  wire MEM_stall;
  
  // ==============================================================================
  // Stage IF
  
  ProgramCounter PC (
    .clock_i(clock_i), .reset_i(reset_i), .run_i(pc_run & !MEM_stall), // TODO: debug
    .pc_i(pc_next),
    .pc_o()
  );
  
  InstructionMemory IMem (
    .addr_i(PC.pc_o),
    .instruction_o()
  );
  
  Adder IF_PCPlus4 (
    .dataA_i(PC.pc_o),
    .dataB_i(4),
    .data_o()
  );
  
  // 
  // TODO: debug
  wire IF_Buf_write = pc_flush || (ID_Hazard.IF_ID_Write_o & !MEM_stall);
  wire IF_Buf_clear = pc_flush;
  
  // Instruction Buffer
  Buffer #(.BITS(32)) IF_Buf_Instruction ( 
    .clock_i(clock_i), .reset_i(reset_i), 
    .write_i(IF_Buf_write),
    .data_i(IMem.instruction_o), .clear_i(IF_Buf_clear), 
    .data_o()
  );
  
  // PC+4 Buffer
  Buffer #(.BITS(32)) IF_Buf_PCPlus4 (
    .clock_i(clock_i), .reset_i(reset_i), 
    .write_i(IF_Buf_write),
    .data_i(IF_PCPlus4.data_o), .clear_i(IF_Buf_clear), 
    .data_o()
  );
  
  // ==============================================================================
  // Stage ID
  
  wire [5:0] ID_opcode =      IF_Buf_Instruction.data_o[31:26];
  wire [4:0] ID_rs_addr =     IF_Buf_Instruction.data_o[25:21];
  wire [4:0] ID_rt_addr =     IF_Buf_Instruction.data_o[20:16];
  wire [4:0] ID_rd_addr =     IF_Buf_Instruction.data_o[15:11];
  wire [4:0] ID_shamt =       IF_Buf_Instruction.data_o[10:6];
  wire [5:0] ID_funct =       IF_Buf_Instruction.data_o[5:0];
  wire [15:0] ID_immed =      IF_Buf_Instruction.data_o[15:0];
  wire [25:0] ID_jmp_target = IF_Buf_Instruction.data_o[25:0];
  wire [31:0] ID_rs_data, ID_rt_data;
  
  // TODO: debug
  wire ID_Buf_write = !control_propagation || !MEM_stall;
  wire ID_Buf_clear = !control_propagation;
  
  // Control
  Control ID_Ctrl( .opcode_i(ID_opcode) );
  
  // Extend Immediate Value to 32-bit
  SignExtender ID_ImmedExt ( .data_i(ID_immed), .data_o() );
  
  // Compute Branch Offset
  LeftShifter ID_ImmedLShift2 ( .data_i(ID_ImmedExt.data_o), .data_o() );
  Adder ID_BranchOffset (
    .dataA_i(ID_ImmedLShift2.data_o), .dataB_i(IF_Buf_PCPlus4.data_o), .data_o()
  );
  
  // Compute Jump Address
  LeftShifter #(.BITS(28)) ID_JTarLShift2 ( .data_i({2'b0, ID_jmp_target}), .data_o() );
  wire [31:0] ID_jump_addr = { ID_BranchSel.data_o[31:28], ID_JTarLShift2.data_o };
  
  // Register
  RegisterFile RegFile( 
    .clock_i(clock_i), 
    .rs_addr_i(ID_rs_addr), .rt_addr_i(ID_rt_addr),
    .rd_addr_i(rd_addr), .rd_data_i(rd_data), .rd_write_i(rd_write),
    .rs_data_o(ID_rs_data), .rt_data_o(ID_rt_data)
  );
  
  // Comparator
  Comparator ID_Compare( .dataA_i(ID_rs_data), .dataB_i(ID_rt_data) );
  
  // Control Buffers
  Buffer #(.BITS(4)) ID_Buf_EX ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(ID_Buf_write), 
    .data_i(ID_Ctrl.ctrl_EX_o), .clear_i(ID_Buf_clear)
  );
  Buffer #(.BITS(2)) ID_Buf_M  ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(ID_Buf_write), 
    .data_i(ID_Ctrl.ctrl_M_o), .clear_i(ID_Buf_clear)
  );
  Buffer #(.BITS(2)) ID_Buf_WB ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(ID_Buf_write), 
    .data_i(ID_Ctrl.ctrl_WB_o), .clear_i(ID_Buf_clear)
  );
  
  // Register Buffers
  Buffer #(.BITS(32)) ID_Buf_RsData ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(ID_Buf_write), 
    .data_i(ID_rs_data),
    .clear_i(0)
  );
  Buffer #(.BITS(32)) ID_Buf_RtData (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(ID_Buf_write), 
    .data_i(ID_rt_data),
    .clear_i(0)
  );
  Buffer #(.BITS(5)) ID_Buf_RsAddr (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(ID_Buf_write), 
    .data_i(ID_rs_addr),
    .clear_i(0)
  );
  Buffer #(.BITS(5)) ID_Buf_RtAddr (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(ID_Buf_write), 
    .data_i(ID_rt_addr),
    .clear_i(0)
  );
  Buffer #(.BITS(5)) ID_Buf_RdAddr (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(ID_Buf_write), 
    .data_i(ID_rd_addr),
    .clear_i(0)
  );
  
  // Immediate Value Buffer
  Buffer #(.BITS(32)) ID_Buf_Immediate (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(ID_Buf_write), 
    .data_i(ID_ImmedExt.data_o), .clear_i(0)
  );
  
  // ==============================================================================
  // Stage EX
  
  wire EX_alusrcB = ID_Buf_EX.data_o[0];
  wire [1:0] EX_aluop = ID_Buf_EX.data_o[2:1];
  wire EX_regdst = ID_Buf_EX.data_o[3];
  
  wire [5:0] EX_funct = ID_Buf_Immediate.data_o[5:0];
  wire [31:0] EX_dataA, EX_dataB;
  
  // TODO: debug
  wire EX_Buf_write = !MEM_stall;
  wire EX_Buf_clear = 0;
  
  // ALU-DataA-Src
  assign EX_dataA = EX_forwarded_rs_data;
  
  // ALU-DataB-Src
  Mux2 #(.BITS(32)) EX_dataB_mux(
    .data0_i(EX_forwarded_rt_data), 
    .data1_i(ID_Buf_Immediate.data_o), 
    .sel_i(EX_alusrcB), 
    .data_o(EX_dataB)
  );
  
  // ALU
  ALUControl EX_ALUCtrl( .funct_i(EX_funct), .aluop_i(EX_aluop) );
  ALU EX_ALU( .ctrl_i(EX_ALUCtrl.ctrl_o), .dataA_i(EX_dataA), .dataB_i(EX_dataB) );
  
  // Rd Address Buffer
  Mux2 #(.BITS(5)) EX_regdst_mux(
    .data0_i(ID_Buf_RtAddr.data_o), .data1_i(ID_Buf_RdAddr.data_o), 
    .sel_i(EX_regdst)
  );
  Buffer #(.BITS(5)) EX_Buf_RdAddr(
    .clock_i(clock_i), .reset_i(reset_i), .write_i(EX_Buf_write), 
    .data_i(EX_regdst_mux.data_o), .clear_i(0)
  );
  
  // Control Buffers
  Buffer #(.BITS(2)) EX_Buf_M  ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(EX_Buf_write), 
    .data_i(ID_Buf_M.data_o), .clear_i(EX_Buf_clear)
  );
  Buffer #(.BITS(2)) EX_Buf_WB ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(EX_Buf_write), 
    .data_i(ID_Buf_WB.data_o), .clear_i(EX_Buf_clear)
  );
  
  // Rt Data Buffer
  Buffer #(.BITS(32)) EX_Buf_RtData (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(EX_Buf_write), 
    .data_i(EX_forwarded_rt_data), .clear_i(0)
  );
  
  // ALU Output Buffer
  Buffer #(.BITS(32)) EX_Buf_ALUOut (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(EX_Buf_write), 
    .data_i(EX_ALU.data_o), .clear_i(0)
  );
  
  // ==============================================================================
  // Stage M
  
  wire M_memread = EX_Buf_M.data_o[0];
  wire M_memwrite = EX_Buf_M.data_o[1];
  
  wire [31:0] M_memdata_o;
  
  // TODO: debug
  wire M_Buf_write = !MEM_stall;
  wire M_Buf_clear = 0;
  
`ifdef USE_CACHE
  
  dcache_top dcache
  (
    // System clock, reset and stall
    .clk_i(clock_i), 
    .rst_i(~reset_i),
	
	// to Data Memory interface		
    .mem_data_i(mem_data_i),
    .mem_ack_i(mem_ack_i),
    .mem_data_o(mem_data_o),
    .mem_addr_o(mem_addr_o),
    .mem_enable_o(mem_enable_o),
    .mem_write_o(mem_write_o),
    
    // to CPU interface	
    .p1_data_i(EX_Buf_RtData.data_o),
    .p1_addr_i(EX_Buf_ALUOut.data_o),
    .p1_MemRead_i(M_memread),
    .p1_MemWrite_i(M_memwrite),
    .p1_data_o(M_memdata_o),
    .p1_stall_o()
  );
  
  assign MEM_stall = dcache.p1_stall_o; // TODO: debug
  
`else
  
  DataMemory_dbg DMem (
    .clock_i(clock_i), 
    .addr_i(EX_Buf_ALUOut.data_o),
    .data_i(EX_Buf_RtData.data_o),
    .read_i(M_memread), .write_i(M_memwrite),
    .data_o(M_memdata_o)
  );
  
  assign MEM_stall = 0;

`endif
  
  // ALU Output Buffer
  Buffer #(.BITS(32)) M_Buf_ALUOut (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(M_Buf_write), 
    .data_i(EX_Buf_ALUOut.data_o), .clear_i(0)
  );
  
  // Rd Address Buffer
  Buffer #(.BITS(5)) M_Buf_RdAddr(
    .clock_i(clock_i), .reset_i(reset_i), .write_i(M_Buf_write), 
    .data_i(EX_Buf_RdAddr.data_o), .clear_i(0)
  );
  
  // Memory Read Buffer
  Buffer #(.BITS(32)) M_Buf_MemData(
    .clock_i(clock_i), .reset_i(reset_i), .write_i(M_Buf_write), 
    .data_i(M_memdata_o), .clear_i(0)
  );
  
  // Control Buffers
  Buffer #(.BITS(2)) M_Buf_WB ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(M_Buf_write), 
    .data_i(EX_Buf_WB.data_o), .clear_i(M_Buf_clear)
  );
  
  // ==============================================================================
  // Stage WB
  
  wire WB_regwrite = M_Buf_WB.data_o[0];
  wire WB_mem2reg = M_Buf_WB.data_o[1];
  
  assign rd_write = WB_regwrite;
  
  Mux2 #(.BITS(32)) WB_RdDataSel(
    .data0_i(M_Buf_ALUOut.data_o), .data1_i(M_Buf_MemData.data_o), .sel_i(WB_mem2reg),
    .data_o(rd_data)
  );
  
  assign rd_addr = M_Buf_RdAddr.data_o;
  
  // ==============================================================================
  // Hazard Detection
  
  wire [4:0] hz_IF_ID_RsAddr = ID_rs_addr;
  wire [4:0] hz_IF_ID_RtAddr = ID_rt_addr;
  wire [4:0] hz_ID_EX_RtAddr = ID_Buf_RtAddr.data_o;
  wire hz_ID_EX_Ctrl_MemRead = ID_Buf_M.data_o[0];
  wire hz_ID_Ctrl_Branch = ID_ctrl_branch;
  wire hz_Flush = pc_flush;
  
  HazardDetection ID_Hazard (
    .ID_EX_MemRead_i(hz_ID_EX_Ctrl_MemRead),
    .ID_Branch_i(hz_ID_Ctrl_Branch),
    .ID_EX_RtAddr_i(hz_ID_EX_RtAddr),
    .IF_ID_RsAddr_i(hz_IF_ID_RsAddr),
    .IF_ID_RtAddr_i(hz_IF_ID_RtAddr),
    .ID_Flush_i(hz_Flush),
    
    .pc_run_o(pc_run),
    .IF_ID_Write_o(),
    .control_propagation_o(control_propagation)
  );
  
  // ==============================================================================
  // Forwarding
  
  wire [4:0] fw_ID_EX_RsAddr = ID_Buf_RsAddr.data_o;
  wire [4:0] fw_ID_EX_RtAddr = ID_Buf_RtAddr.data_o;
  wire [4:0] fw_EX_M_RdAddr = EX_Buf_RdAddr.data_o;
  wire [4:0] fw_M_WB_RdAddr = M_Buf_RdAddr.data_o;
  wire fw_EX_M_RdWrite = EX_Buf_WB.data_o[0];
  wire fw_M_WB_RdWrite = M_Buf_WB.data_o[0];
  
  wire [31:0] fw_RsData = ID_Buf_RsData.data_o;
  wire [31:0] fw_RtData = ID_Buf_RtData.data_o;
  wire [31:0] fw_ALUOut = EX_Buf_ALUOut.data_o;
  wire [31:0] fw_RdData = rd_data;
  
  ForwardingUnit EX_Forwarding (
    .EX_M_RegWrite_i(fw_EX_M_RdWrite),
    .EX_M_RdAddr_i(fw_EX_M_RdAddr),
    .M_WB_RegWrite_i(fw_M_WB_RdWrite),
    .M_WB_RdAddr_i(fw_M_WB_RdAddr),
    .ID_EX_RsAddr_i(fw_ID_EX_RsAddr),
    .ID_EX_RtAddr_i(fw_ID_EX_RtAddr),
    .Forward_Rs_o(),
    .Forward_Rt_o()
  );
  
  Mux3 EX_RsForward(
    .data0_i(fw_RsData),
    .data1_i(fw_RdData),
    .data2_i(fw_ALUOut),
    .sel_i(EX_Forwarding.Forward_Rs_o),
    .data_o(EX_forwarded_rs_data)
  );
  
  Mux3 EX_RtForward(
    .data0_i(fw_RtData),
    .data1_i(fw_RdData),
    .data2_i(fw_ALUOut),
    .sel_i(EX_Forwarding.Forward_Rt_o),
    .data_o(EX_forwarded_rt_data)
  );
  
  // ==============================================================================
  // Compute PC Next
  
  wire ID_eq = ID_Compare.equal_o;
  wire ID_ctrl_jump = ID_Ctrl.ctrl_J_o[0];
  wire ID_ctrl_branch = ID_Ctrl.ctrl_J_o[1];
  
  Mux2 #(.BITS(32)) ID_BranchSel( 
    .data0_i(IF_PCPlus4.data_o), 
    .data1_i(ID_BranchOffset.data_o), 
    .sel_i( ID_ctrl_branch & ID_eq ), 
    .data_o()
  );
  Mux2 #(.BITS(32)) ID_JumpSel(
    .data0_i(ID_BranchSel.data_o),
    .data1_i(ID_jump_addr), 
    .sel_i(ID_ctrl_jump), 
    .data_o(pc_next)
  );
  
  // 
  assign pc_flush = ID_ctrl_jump | ( ID_ctrl_branch & ID_eq );
  
endmodule
