module SignExtender ( data_i, data_o );
  // IO Bits
  parameter INPUT_BITS = 16;
  parameter OUTPUT_BITS = 32;
  
  // Input
  input [INPUT_BITS-1:0] data_i;
  
  // Output
  output [OUTPUT_BITS-1:0] data_o;
  
  // Simple Assignment
  assign data_o = { 
                    {(OUTPUT_BITS-INPUT_BITS){ data_i[INPUT_BITS-1] }}, 
                    data_i
                  };
  
endmodule
