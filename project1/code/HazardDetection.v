module HazardDetection (
  ID_EX_MemRead_i,
  ID_Branch_i,
  ID_EX_RtAddr_i,
  IF_ID_RsAddr_i,
  IF_ID_RtAddr_i,
  
  ID_Flush_i,
  
  pc_run_o,
  IF_ID_Write_o,
  control_propagation_o
);
  
  input ID_EX_MemRead_i, ID_Branch_i;
  input [4:0] ID_EX_RtAddr_i, IF_ID_RsAddr_i, IF_ID_RtAddr_i;
  input ID_Flush_i;
  
  output pc_run_o, IF_ID_Write_o, control_propagation_o;
  reg pc_run_o, IF_ID_Write_o, control_propagation_o;
  
  always @ ( 
    ID_EX_MemRead_i or ID_Branch_i or 
    ID_EX_RtAddr_i or IF_ID_RsAddr_i or IF_ID_RtAddr_i or
    ID_Flush_i
  )
  begin
    
    if ((ID_EX_MemRead_i || ID_Branch_i) && 
        (ID_EX_RtAddr_i != 0) && 
        ((ID_EX_RtAddr_i == IF_ID_RsAddr_i) || (ID_EX_RtAddr_i == IF_ID_RtAddr_i)))
      begin // Load/Branch Stall
        pc_run_o = 0;
        IF_ID_Write_o = 0;
        control_propagation_o = 0;
      end
    else if (ID_Flush_i)
      begin // Flush
        pc_run_o = 1;
        IF_ID_Write_o = 1;
        control_propagation_o = 0;
      end
    else
      begin
        pc_run_o = 1;
        IF_ID_Write_o = 1;
        control_propagation_o = 1;
      end
    
  end
  
endmodule
