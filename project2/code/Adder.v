module Adder ( dataA_i, dataB_i, data_o );
  // Data Bits
  parameter BITS = 32;
  
  // Input
  input [BITS-1:0] dataA_i, dataB_i;
  
  // Output
  output [BITS-1:0] data_o;
  
  // Simple Assignment 
  assign data_o = dataA_i + dataB_i;
  
endmodule

