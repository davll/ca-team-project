module RegisterFile ( 
  clock_i, 
  rs_addr_i, rt_addr_i, 
  rd_addr_i, rd_data_i, rd_write_i, 
  rs_data_o, rt_data_o 
);
  
  parameter ADDR_BITS = 5;
  parameter WORD_BITS = 32;
  parameter SIZE = 1 << ADDR_BITS;
  
  input clock_i;
  input [ADDR_BITS-1:0] rs_addr_i, rt_addr_i, rd_addr_i;
  input [WORD_BITS-1:0] rd_data_i;
  input rd_write_i;
  
  output [WORD_BITS-1:0] rs_data_o, rt_data_o;
  
  // Register
  reg [WORD_BITS-1:0] _content [0:SIZE-1];
  
  // Read
  assign rs_data_o = _content[rs_addr_i];
  assign rt_data_o = _content[rt_addr_i];
  
  // Write
  always @ (posedge clock_i)
  begin
    if(rd_write_i && (rd_addr_i != 0))
      _content[rd_addr_i] = rd_data_i;
  end
  
endmodule

