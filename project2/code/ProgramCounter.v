module ProgramCounter ( clock_i, reset_i, run_i, pc_i, pc_o );
  // Bits
  parameter ADDR_BITS = 32;
  
  // Input
  input clock_i, reset_i, run_i;
  input [ADDR_BITS-1:0] pc_i;
  
  // Output
  output [ADDR_BITS-1:0] pc_o;
  reg [ADDR_BITS-1:0] pc_o;
  
  // 
  always @ ( posedge clock_i or posedge reset_i )
  begin
    if (reset_i)
      pc_o <= 0;
    else
      pc_o <= (run_i) ? pc_i : pc_o;
  end
  
endmodule

