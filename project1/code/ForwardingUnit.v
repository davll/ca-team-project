module ForwardingUnit (
  EX_M_RegWrite_i,
  EX_M_RdAddr_i,
  
  M_WB_RegWrite_i,
  M_WB_RdAddr_i,
  
  ID_EX_RsAddr_i,
  ID_EX_RtAddr_i,
  
  Forward_Rs_o,
  Forward_Rt_o
);
  
  input EX_M_RegWrite_i, M_WB_RegWrite_i;
  input [4:0] EX_M_RdAddr_i, M_WB_RdAddr_i, ID_EX_RsAddr_i, ID_EX_RtAddr_i;
  
  output [1:0] Forward_Rs_o, Forward_Rt_o;
  reg [1:0] Forward_Rs_o, Forward_Rt_o;
  
  always @ ( 
    EX_M_RegWrite_i or M_WB_RegWrite_i or EX_M_RdAddr_i or M_WB_RdAddr_i or
    ID_EX_RsAddr_i or ID_EX_RtAddr_i
  )
  begin
    
    // Data in EX_M Buffer is newer than in M_WB Buffer
    
    if (EX_M_RegWrite_i && (EX_M_RdAddr_i!=0) && (EX_M_RdAddr_i === ID_EX_RsAddr_i))
      Forward_Rs_o = 2;
    else if(M_WB_RegWrite_i && (M_WB_RdAddr_i!=0) && (M_WB_RdAddr_i === ID_EX_RsAddr_i))
      Forward_Rs_o = 1;
    else
      Forward_Rs_o = 0;
    
    if (EX_M_RegWrite_i && (EX_M_RdAddr_i!=0) && (EX_M_RdAddr_i === ID_EX_RtAddr_i))
      Forward_Rt_o = 2;
    else if(M_WB_RegWrite_i && (M_WB_RdAddr_i!=0) && (M_WB_RdAddr_i === ID_EX_RtAddr_i))
      Forward_Rt_o = 1;
    else
      Forward_Rt_o = 0;
    
  end
  
endmodule
