`define CYCLE_TIME 50

module main;
  
  reg clock, reset, start;
  integer i, outfile, counter, nstall, nflush;
  
  always #(`CYCLE_TIME/2) clock = ~clock;
  
  CPU cpu ( clock, reset, start );
  
  initial
  begin
    counter = 0;
    nstall = 0;
    nflush = 0;
    
    // Initialize Instruction Memory
    for (i=0; i<cpu.IMem.SIZE; i=i+1)
    begin
      cpu.IMem._content[i] = 0;
    end
    
    // Initialize Data Memory
    // TODO
    for (i=0; i<cpu.DMem.SIZE; i=i+1)
    begin
      cpu.DMem._content[i] = 0;
    end
    
    // Initialize Register File
    // TODO
    for (i=0; i<cpu.RegFile.SIZE; i=i+1)
    begin
      cpu.RegFile._content[i] = 0;
    end
    
    // Read Instruction Code
    $readmemb("instruction.txt", cpu.IMem._content);
    
    // Open Output File
    outfile = $fopen("output.txt") | 1;
    
    // Set Input n into data memory at 0x00
    cpu.DMem._content[0] = 8'h5;       // n = 5 for example
    
    // Clearing
    clock = 1;
    reset = 1;
    start = 0;
    
    // Start
    #(`CYCLE_TIME/4)
    reset = 0;
    start = 1;
  end
  
  always @ (negedge clock)
  begin
    //if (counter >= 128)
    //  $finish;
    if ((cpu.PC.pc_o>>2) >= cpu.IMem.SIZE)
      $finish;
    
    $fdisplay(outfile, "cycle = %d, Start = %d, Stall = %d, Flush = %d", 
              counter, (counter > 0 && start), nstall, nflush);
    
    // Print PC
    $fdisplay(outfile, "PC = %d", cpu.PC.pc_o);
    
    // Debug Output
    if (0)
    begin
      $fdisplay(outfile, "== IF stage ==");
      $fdisplay(outfile, "IF_PCPlus4.data_o = %b", cpu.IF_PCPlus4.data_o);
      $fdisplay(outfile, "IMem.instruction_o = %b", cpu.IMem.instruction_o);
      
      $fdisplay(outfile, "==============");
      $fdisplay(outfile, "IF/ID Instruction: %b", cpu.IF_Buf_Instruction.data_o);
      $fdisplay(outfile, "IF/ID PC+4: %b", cpu.IF_Buf_PCPlus4.data_o);
      
      $fdisplay(outfile, "== ID stage ==");
      $fdisplay(outfile, "Jump Control: %b", cpu.ID_Ctrl.ctrl_J_o);
      $fdisplay(outfile, "PC Flush: %b", cpu.pc_flush);
      $fdisplay(outfile, "IF_bufs_write: %b", cpu.IF_bufs_write);
      
      $fdisplay(outfile, "==============");
      $fdisplay(outfile, "ID/EX Controls: EX=%b M=%b WB=%b", 
                cpu.ID_Buf_EX.data_o, cpu.ID_Buf_M.data_o, cpu.ID_Buf_WB.data_o);
      $fdisplay(outfile, "ID_Buf_RsData = %b", cpu.ID_Buf_RsData.data_o);
      $fdisplay(outfile, "ID_Buf_RtData = %b", cpu.ID_Buf_RtData.data_o);
      $fdisplay(outfile, "ID_Buf_RsAddr = %b", cpu.ID_Buf_RsAddr.data_o);
      $fdisplay(outfile, "ID_Buf_RtAddr = %b", cpu.ID_Buf_RtAddr.data_o);
      $fdisplay(outfile, "ID_Buf_RdAddr = %b", cpu.ID_Buf_RdAddr.data_o);
      $fdisplay(outfile, "ID_Buf_Immediate = %b", cpu.ID_Buf_Immediate.data_o);
      
      $fdisplay(outfile, "== EX stage ==");
      
      $fdisplay(outfile, "==============");
      $fdisplay(outfile, "EX/M Controls: M=%b WB=%b", 
                cpu.EX_Buf_M.data_o, cpu.EX_Buf_WB.data_o);
      $fdisplay(outfile, "EX_Buf_RdAddr = %b", cpu.EX_Buf_RdAddr.data_o);
      $fdisplay(outfile, "EX_Buf_RtData = %b", cpu.EX_Buf_RtData.data_o);
      $fdisplay(outfile, "EX_Buf_ALUOut = %b", cpu.EX_Buf_ALUOut.data_o);
      
      $fdisplay(outfile, "== M stage ==");
      
      $fdisplay(outfile, "==============");
      $fdisplay(outfile, "M/WB Controls: WB=%b", cpu.M_Buf_WB.data_o);
      $fdisplay(outfile, "M_Buf_ALUOut = %b", cpu.M_Buf_ALUOut.data_o);
      $fdisplay(outfile, "M_Buf_RdAddr = %b", cpu.M_Buf_RdAddr.data_o);
      $fdisplay(outfile, "M_Buf_MemData = %b", cpu.M_Buf_MemData.data_o);
      
      $fdisplay(outfile, "== WB stage ==");
      $fdisplay(outfile, "WB_regwrite = %b, WB_mem2reg =%b", 
                         cpu.WB_regwrite, cpu.WB_mem2reg);
    end
      
    // Print Registers
    $fdisplay(outfile, "Registers");
    $fdisplay(outfile, "R0(r0) = %d, R8 (t0) = %d, R16(s0) = %d, R24(t8) = %d", 
              cpu.RegFile._content[0], cpu.RegFile._content[8] , 
              cpu.RegFile._content[16], cpu.RegFile._content[24]);
    $fdisplay(outfile, "R1(at) = %d, R9 (t1) = %d, R17(s1) = %d, R25(t9) = %d", 
              cpu.RegFile._content[1], cpu.RegFile._content[9] , 
              cpu.RegFile._content[17], cpu.RegFile._content[25]);
    $fdisplay(outfile, "R2(v0) = %d, R10(t2) = %d, R18(s2) = %d, R26(k0) = %d", 
              cpu.RegFile._content[2], cpu.RegFile._content[10], 
              cpu.RegFile._content[18], cpu.RegFile._content[26]);
    $fdisplay(outfile, "R3(v1) = %d, R11(t3) = %d, R19(s3) = %d, R27(k1) = %d", 
              cpu.RegFile._content[3], cpu.RegFile._content[11], 
              cpu.RegFile._content[19], cpu.RegFile._content[27]);
    $fdisplay(outfile, "R4(a0) = %d, R12(t4) = %d, R20(s4) = %d, R28(gp) = %d", 
              cpu.RegFile._content[4], cpu.RegFile._content[12], 
              cpu.RegFile._content[20], cpu.RegFile._content[28]);
    $fdisplay(outfile, "R5(a1) = %d, R13(t5) = %d, R21(s5) = %d, R29(sp) = %d", 
              cpu.RegFile._content[5], cpu.RegFile._content[13], 
              cpu.RegFile._content[21], cpu.RegFile._content[29]);
    $fdisplay(outfile, "R6(a2) = %d, R14(t6) = %d, R22(s6) = %d, R30(s8) = %d", 
              cpu.RegFile._content[6], cpu.RegFile._content[14], 
              cpu.RegFile._content[22], cpu.RegFile._content[30]);
    $fdisplay(outfile, "R7(a3) = %d, R15(t7) = %d, R23(s7) = %d, R31(ra) = %d", 
              cpu.RegFile._content[7], cpu.RegFile._content[15], 
              cpu.RegFile._content[23], cpu.RegFile._content[31]);
    
    // Print Data Memory
    // TODO
    $fdisplay(outfile, "Data Memory: 0x00 = %d", {cpu.DMem._content[3] , 
                                                  cpu.DMem._content[2] , 
                                                  cpu.DMem._content[1] , 
                                                  cpu.DMem._content[0] });
    
    $fdisplay(outfile, "Data Memory: 0x04 = %d", {cpu.DMem._content[7] , 
                                                  cpu.DMem._content[6] , 
                                                  cpu.DMem._content[5] , 
                                                  cpu.DMem._content[4] });
    
    $fdisplay(outfile, "Data Memory: 0x08 = %d", {cpu.DMem._content[11], 
                                                  cpu.DMem._content[10], 
                                                  cpu.DMem._content[9] , 
                                                  cpu.DMem._content[8] });
    
    $fdisplay(outfile, "Data Memory: 0x0c = %d", {cpu.DMem._content[15], 
                                                  cpu.DMem._content[14], 
                                                  cpu.DMem._content[13], 
                                                  cpu.DMem._content[12]});
    
    $fdisplay(outfile, "Data Memory: 0x10 = %d", {cpu.DMem._content[19], 
                                                  cpu.DMem._content[18], 
                                                  cpu.DMem._content[17], 
                                                  cpu.DMem._content[16]});
    
    $fdisplay(outfile, "Data Memory: 0x14 = %d", {cpu.DMem._content[23], 
                                                  cpu.DMem._content[22], 
                                                  cpu.DMem._content[21], 
                                                  cpu.DMem._content[20]});
    
    $fdisplay(outfile, "Data Memory: 0x18 = %d", {cpu.DMem._content[27], 
                                                  cpu.DMem._content[26], 
                                                  cpu.DMem._content[25], 
                                                  cpu.DMem._content[24]});
    
    $fdisplay(outfile, "Data Memory: 0x1c = %d", {cpu.DMem._content[31], 
                                                  cpu.DMem._content[30], 
                                                  cpu.DMem._content[29], 
                                                  cpu.DMem._content[28]});
    
    $fdisplay(outfile, "\n");
    counter = counter + 1;
    if (!cpu.control_propagation && !cpu.pc_flush)
      nstall = nstall + 1;
    if (cpu.pc_flush)
      nflush = nflush + 1;
  end
  
endmodule