`include "Defs.v"

module ALUControl ( funct_i, aluop_i, ctrl_o );
  
  localparam Funct_Add = 6'b100000;
  localparam Funct_Sub = 6'b100010;
  localparam Funct_And = 6'b100100;
  localparam Funct_Or =  6'b100101;
  localparam Funct_Mul = 6'b011000;
  
  input [5:0] funct_i;
  input [`ALUOP_BITS-1:0] aluop_i;
  
  output [`ALUCTRL_BITS-1:0] ctrl_o;
  
  reg [`ALUCTRL_BITS-1:0] ctrl_o;
  
  always @ (funct_i or aluop_i)
  begin
    if (aluop_i === `ALUOP_FUNCT)
      case (funct_i)
        Funct_Add: ctrl_o = `ALUCTRL_ADD;
        Funct_Sub: ctrl_o = `ALUCTRL_SUB;
        Funct_And: ctrl_o = `ALUCTRL_AND;
        Funct_Or:  ctrl_o = `ALUCTRL_OR;
        Funct_Mul: ctrl_o = `ALUCTRL_MUL;
      endcase
    else if (aluop_i === `ALUOP_ADD)
      ctrl_o = `ALUCTRL_ADD;
  end
  
endmodule

