module Comparator (
  dataA_i, dataB_i,
  equal_o
);
  parameter BITS = 32;
  
  input [BITS-1:0] dataA_i, dataB_i;
  
  output equal_o;
  
  assign equal_o = dataA_i === dataB_i;
  
endmodule
