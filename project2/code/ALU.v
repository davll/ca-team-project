`include "Defs.v"

module ALU (
  ctrl_i, dataA_i, dataB_i, data_o
);
  
  // Data Bits
  parameter BITS = 32;
  
  // Input
  input [`ALUCTRL_BITS-1:0] ctrl_i;
  input [BITS-1:0] dataA_i, dataB_i;
  
  // Output
  output [BITS-1:0] data_o;
  
  // Output Space
  reg [BITS-1:0] data_o;
  
  always @ ( dataA_i or dataB_i or ctrl_i )
  begin
    case (ctrl_i)
      `ALUCTRL_ADD: data_o = dataA_i + dataB_i;
      `ALUCTRL_SUB: data_o = dataA_i - dataB_i;
      `ALUCTRL_AND: data_o = dataA_i & dataB_i;
      `ALUCTRL_OR:  data_o = dataA_i | dataB_i;
      `ALUCTRL_MUL: data_o = dataA_i * dataB_i;
    endcase
  end
  
endmodule

