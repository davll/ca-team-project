module Mux2 ( data0_i, data1_i, sel_i, data_o );
  // Data Bits
  parameter BITS = 32;
  
  // Input
  input [BITS-1:0] data0_i, data1_i;
  input sel_i;
  
  // Output
  output [BITS-1:0] data_o;
  
  // Simple Assignment
  assign data_o = ( sel_i ? data1_i : data0_i );
  
endmodule

module Mux3 ( data0_i, data1_i, data2_i, data3_i, sel_i, data_o );
  // Data Bits
  parameter BITS = 32;
  
  // Input
  input [BITS-1:0] data0_i, data1_i, data2_i, data3_i;
  input [1:0] sel_i;
  
  // Output
  output [BITS-1:0] data_o;
  
  // Simple Assignment
  assign data_o = (sel_i[1] ? 
                   (sel_i[0] ? data3_i : data2_i ) : 
                   (sel_i[0] ? data1_i : data0_i )
                  );
  
endmodule
