module LeftShifter ( data_i, data_o );
  // Data Bits
  parameter BITS = 32;
  
  // Input
  input [BITS-1:0] data_i;
  
  // Output
  output [BITS-1:0] data_o;
  
  // Simple Assignment
  assign data_o = { data_i[BITS-3:0], 2'd0 };
  
endmodule
