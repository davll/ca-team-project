`include "Defs.v"

module CPU ( clock_i, reset_i, start_i );
  
  input clock_i, reset_i, start_i;
  
  //
  wire pc_run, control_propagation, IF_bufs_write, pc_flush;
  wire [31:0] pc_next;
  
  //
  wire [4:0] rd_addr;
  wire [31:0] rd_data;
  wire rd_write;
  
  //
  wire [31:0] EX_forwarded_rs_data, EX_forwarded_rt_data;
  
  // ==============================================================================
  // Stage IF
  
  ProgramCounter PC (
    .clock_i(clock_i), .reset_i(reset_i), .run_i(pc_run),
    .pc_i(pc_next),
    .pc_o()
  );
  
  InstructionMemory IMem (
    .addr_i(PC.pc_o),
    .instruction_o()
  );
  
  Adder IF_PCPlus4 (
    .dataA_i(PC.pc_o),
    .dataB_i(4),
    .data_o()
  );
  
  // Instruction Buffer
  Buffer #(.BITS(32)) IF_Buf_Instruction ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(IF_bufs_write),
    .data_i( pc_flush ? 0 : IMem.instruction_o ),
    .data_o()
  );
  
  // PC+4 Buffer
  Buffer #(.BITS(32)) IF_Buf_PCPlus4 (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(IF_bufs_write),
    .data_i( pc_flush ? 0 : IF_PCPlus4.data_o ), 
    .data_o()
  );
  
  // ==============================================================================
  // Stage ID
  
  wire [5:0] ID_opcode =      IF_Buf_Instruction.data_o[31:26];
  wire [4:0] ID_rs_addr =     IF_Buf_Instruction.data_o[25:21];
  wire [4:0] ID_rt_addr =     IF_Buf_Instruction.data_o[20:16];
  wire [4:0] ID_rd_addr =     IF_Buf_Instruction.data_o[15:11];
  wire [4:0] ID_shamt =       IF_Buf_Instruction.data_o[10:6];
  wire [5:0] ID_funct =       IF_Buf_Instruction.data_o[5:0];
  wire [15:0] ID_immed =      IF_Buf_Instruction.data_o[15:0];
  wire [25:0] ID_jmp_target = IF_Buf_Instruction.data_o[25:0];
  wire [31:0] ID_rs_data, ID_rt_data;
  
  // Control
  Control ID_Ctrl( .opcode_i(ID_opcode) );
  
  // Extend Immediate Value to 32-bit
  SignExtender ID_ImmedExt ( .data_i(ID_immed), .data_o() );
  
  // Compute Branch Offset
  LeftShifter ID_ImmedLShift2 ( .data_i(ID_ImmedExt.data_o), .data_o() );
  Adder ID_BranchOffset (
    .dataA_i(ID_ImmedLShift2.data_o), .dataB_i(IF_Buf_PCPlus4.data_o), .data_o()
  );
  
  // Compute Jump Address
  LeftShifter #(.BITS(28)) ID_JTarLShift2 ( .data_i({2'b0, ID_jmp_target}), .data_o() );
  wire [31:0] ID_jump_addr = { ID_BranchSel.data_o[31:28], ID_JTarLShift2.data_o };
  
  // Register
  RegisterFile RegFile( 
    .clock_i(clock_i), 
    .rs_addr_i(ID_rs_addr), .rt_addr_i(ID_rt_addr),
    .rd_addr_i(rd_addr), .rd_data_i(rd_data), .rd_write_i(rd_write),
    .rs_data_o(ID_rs_data), .rt_data_o(ID_rt_data)
  );
  
  // Comparator
  Comparator ID_Compare( .dataA_i(ID_rs_data), .dataB_i(ID_rt_data) );
  
  // Control Buffers
  Buffer #(.BITS(4)) ID_Buf_EX ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), 
    .data_i( control_propagation ? ID_Ctrl.ctrl_EX_o : 0 )
  );
  Buffer #(.BITS(2)) ID_Buf_M  ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), 
    .data_i( control_propagation ? ID_Ctrl.ctrl_M_o : 0 )
  );
  Buffer #(.BITS(2)) ID_Buf_WB ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), 
    .data_i( control_propagation ? ID_Ctrl.ctrl_WB_o : 0 )
  );
  
  // Register Buffers
  Buffer #(.BITS(32)) ID_Buf_RsData ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(ID_rs_data) 
  );
  Buffer #(.BITS(32)) ID_Buf_RtData (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(ID_rt_data)
  );
  Buffer #(.BITS(5)) ID_Buf_RsAddr (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(ID_rs_addr)
  );
  Buffer #(.BITS(5)) ID_Buf_RtAddr (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(ID_rt_addr)
  );
  Buffer #(.BITS(5)) ID_Buf_RdAddr (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(ID_rd_addr)
  );
  
  // Immediate Value Buffer
  Buffer #(.BITS(32)) ID_Buf_Immediate (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(ID_ImmedExt.data_o)
  );
  
  // ==============================================================================
  // Stage EX
  
  wire EX_alusrcB = ID_Buf_EX.data_o[0];
  wire [1:0] EX_aluop = ID_Buf_EX.data_o[2:1];
  wire EX_regdst = ID_Buf_EX.data_o[3];
  
  wire [5:0] EX_funct = ID_Buf_Immediate.data_o[5:0];
  wire [31:0] EX_dataA, EX_dataB;
  
  // ALU-DataA-Src
  assign EX_dataA = EX_forwarded_rs_data;
  
  // ALU-DataB-Src
  Mux2 #(.BITS(32)) EX_dataB_mux(
    .data0_i(EX_forwarded_rt_data), 
    .data1_i(ID_Buf_Immediate.data_o), 
    .sel_i(EX_alusrcB), 
    .data_o(EX_dataB)
  );
  
  // ALU
  ALUControl EX_ALUCtrl( .funct_i(EX_funct), .aluop_i(EX_aluop) );
  ALU EX_ALU( .ctrl_i(EX_ALUCtrl.ctrl_o), .dataA_i(EX_dataA), .dataB_i(EX_dataB) );
  
  // Rd Address Buffer
  Mux2 #(.BITS(5)) EX_regdst_mux(
    .data0_i(ID_Buf_RtAddr.data_o), .data1_i(ID_Buf_RdAddr.data_o), .sel_i(EX_regdst)
  );
  Buffer #(.BITS(5)) EX_Buf_RdAddr(
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(EX_regdst_mux.data_o)
  );
  
  // Control Buffers
  Buffer #(.BITS(2)) EX_Buf_M  ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(ID_Buf_M.data_o)
  );
  Buffer #(.BITS(2)) EX_Buf_WB ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(ID_Buf_WB.data_o)
  );
  
  // Rt Data Buffer
  Buffer #(.BITS(32)) EX_Buf_RtData (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(EX_forwarded_rt_data)
  );
  
  // ALU Output Buffer
  Buffer #(.BITS(32)) EX_Buf_ALUOut (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(EX_ALU.data_o)
  );
  
  // ==============================================================================
  // Stage M
  
  wire M_memread = EX_Buf_M.data_o[0];
  wire M_memwrite = EX_Buf_M.data_o[1];
  
  DataMemory DMem (
    .clock_i(clock_i), 
    .addr_i(EX_Buf_ALUOut.data_o),
    .data_i(EX_Buf_RtData.data_o),
    .read_i(M_memread), .write_i(M_memwrite)
  );
  
  // ALU Output Buffer
  Buffer #(.BITS(32)) M_Buf_ALUOut (
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(EX_Buf_ALUOut.data_o)
  );
  
  // Rd Address Buffer
  Buffer #(.BITS(5)) M_Buf_RdAddr(
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(EX_Buf_RdAddr.data_o)
  );
  
  // Memory Read Buffer
  Buffer #(.BITS(32)) M_Buf_MemData(
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(DMem.data_o)
  );
  
  // Control Buffers
  Buffer #(.BITS(2)) M_Buf_WB ( 
    .clock_i(clock_i), .reset_i(reset_i), .write_i(1), .data_i(EX_Buf_WB.data_o)
  );
  
  // ==============================================================================
  // Stage WB
  
  wire WB_regwrite = M_Buf_WB.data_o[0];
  wire WB_mem2reg = M_Buf_WB.data_o[1];
  
  assign rd_write = WB_regwrite;
  
  Mux2 #(.BITS(32)) WB_RdDataSel(
    .data0_i(M_Buf_ALUOut.data_o), .data1_i(M_Buf_MemData.data_o), .sel_i(WB_mem2reg),
    .data_o(rd_data)
  );
  
  assign rd_addr = M_Buf_RdAddr.data_o;
  
  // ==============================================================================
  // Hazard Detection
  
  wire [4:0] hz_IF_ID_RsAddr = ID_rs_addr;
  wire [4:0] hz_IF_ID_RtAddr = ID_rt_addr;
  wire [4:0] hz_ID_EX_RtAddr = ID_Buf_RtAddr.data_o;
  wire hz_ID_EX_Ctrl_MemRead = ID_Buf_M.data_o[0];
  wire hz_ID_Ctrl_Branch = ID_ctrl_branch;
  //wire hz_ID_EX_Ctrl_RegWrite = ID_Buf_WB.data_o[0];
  wire hz_Flush = pc_flush;
  
  //assign IF_bufs_write = 1; // FIXME: determine whether to update IF_ID buffer
  //assign pc_run = 1; // FIXME: determine whether to update PC
  //assign control_propagation = 1; // FIXME: determine to update ID_EX buffer or fill 0
  
  HazardDetection ID_Hazard (
    .ID_EX_MemRead_i(hz_ID_EX_Ctrl_MemRead),
    .ID_Branch_i(hz_ID_Ctrl_Branch),
    .ID_EX_RtAddr_i(hz_ID_EX_RtAddr),
    .IF_ID_RsAddr_i(hz_IF_ID_RsAddr),
    .IF_ID_RtAddr_i(hz_IF_ID_RtAddr),
    .ID_Flush_i(hz_Flush),
    
    .pc_run_o(pc_run),
    .IF_ID_Write_o(IF_bufs_write),
    .control_propagation_o(control_propagation)
  );
  
  // ==============================================================================
  // Forwarding
  
  wire [4:0] fw_ID_EX_RsAddr = ID_Buf_RsAddr.data_o;
  wire [4:0] fw_ID_EX_RtAddr = ID_Buf_RtAddr.data_o;
  wire [4:0] fw_EX_M_RdAddr = EX_Buf_RdAddr.data_o;
  wire [4:0] fw_M_WB_RdAddr = M_Buf_RdAddr.data_o;
  wire fw_EX_M_RdWrite = EX_Buf_WB.data_o[0];
  wire fw_M_WB_RdWrite = M_Buf_WB.data_o[0];
  //wire fw_EX_M_MemtoReg = EX_Buf_WB.data_o[1];
  //wire fw_M_WB_MemtoReg = M_Buf_WB.data_o[1];
  
  wire [31:0] fw_RsData = ID_Buf_RsData.data_o;
  wire [31:0] fw_RtData = ID_Buf_RtData.data_o;
  wire [31:0] fw_ALUOut = EX_Buf_ALUOut.data_o;
  wire [31:0] fw_RdData = rd_data;
  
  //assign EX_forwarded_rs_data = fw_RsData; // FIXME
  //assign EX_forwarded_rt_data = fw_RtData; // FIXME
  
  ForwardingUnit EX_Forwarding (
    .EX_M_RegWrite_i(fw_EX_M_RdWrite),
    .EX_M_RdAddr_i(fw_EX_M_RdAddr),
    .M_WB_RegWrite_i(fw_M_WB_RdWrite),
    .M_WB_RdAddr_i(fw_M_WB_RdAddr),
    .ID_EX_RsAddr_i(fw_ID_EX_RsAddr),
    .ID_EX_RtAddr_i(fw_ID_EX_RtAddr),
    .Forward_Rs_o(),
    .Forward_Rt_o()
  );
  
  Mux3 EX_RsForward(
    .data0_i(fw_RsData),
    .data1_i(fw_RdData),
    .data2_i(fw_ALUOut),
    .sel_i(EX_Forwarding.Forward_Rs_o),
    .data_o(EX_forwarded_rs_data)
  );
  
  Mux3 EX_RtForward(
    .data0_i(fw_RtData),
    .data1_i(fw_RdData),
    .data2_i(fw_ALUOut),
    .sel_i(EX_Forwarding.Forward_Rt_o),
    .data_o(EX_forwarded_rt_data)
  );
  
  // ==============================================================================
  // Compute PC Next
  
  wire ID_eq = ID_Compare.equal_o;
  wire ID_ctrl_jump = ID_Ctrl.ctrl_J_o[0];
  wire ID_ctrl_branch = ID_Ctrl.ctrl_J_o[1];
  
  Mux2 #(.BITS(32)) ID_BranchSel( 
    .data0_i(IF_PCPlus4.data_o), 
    .data1_i(ID_BranchOffset.data_o), 
    .sel_i( ID_ctrl_branch & ID_eq ), 
    .data_o()
  );
  Mux2 #(.BITS(32)) ID_JumpSel(
    .data0_i(ID_BranchSel.data_o),
    .data1_i(ID_jump_addr), 
    .sel_i(ID_ctrl_jump), 
    .data_o(pc_next)
  );
  
  // 
  assign pc_flush = ID_ctrl_jump | ( ID_ctrl_branch & ID_eq );
  
endmodule
