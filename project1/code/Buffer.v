module Buffer ( clock_i, reset_i, write_i, data_i, data_o );
  
  // Data Bits
  parameter BITS = 32;
  
  // Input
  input clock_i, reset_i, write_i;
  input [BITS-1:0] data_i;
  
  // Output
  output [BITS-1:0] data_o;
  
  // Multi Cycle Version:
  
  reg [BITS-1:0] data_o;
  
  always @ ( posedge clock_i or posedge reset_i )
  begin
    if (reset_i)
      data_o <= 0;
    else if (write_i)
      data_o <= data_i;
  end
  
  // Single Cycle Version:
  //assign data_o = data_i;
  
endmodule
