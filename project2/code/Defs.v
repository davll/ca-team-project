`define ALUOP_BITS     2
`define ALUOP_ADD      2'b00
`define ALUOP_FUNCT    2'b01

`define ALUCTRL_BITS   3
`define ALUCTRL_ADD    3'b000
`define ALUCTRL_SUB    3'b001
`define ALUCTRL_AND    3'b010
`define ALUCTRL_OR     3'b011
`define ALUCTRL_MUL    3'b100

`define USE_CACHE 1

