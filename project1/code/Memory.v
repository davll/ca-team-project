// Memory

// Instruction Memory
module InstructionMemory ( addr_i, instruction_o );
  
  parameter ADDR_BITS = 32;
  parameter WORD_BITS = 32;
  parameter SIZE = 256;
  
  input [ADDR_BITS-1:0] addr_i;
  output [WORD_BITS-1:0] instruction_o;
  
  // Memory
  reg [WORD_BITS-1:0] _content [0:SIZE-1];
  
  // Read
  assign instruction_o = _content[ addr_i >> 2 ];
  
endmodule

// Data Memory
module DataMemory 
( clock_i, addr_i, data_i, read_i, write_i, data_o );
  
  parameter ADDR_BITS = 32;
  parameter WORD_BITS = 32;
  parameter SIZE = 32;
  
  input clock_i;
  input [ADDR_BITS-1:0] addr_i;
  input [WORD_BITS-1:0] data_i;
  input read_i, write_i;
  
  output [WORD_BITS-1:0] data_o;
  
  // Memory
  reg [7:0] _content [0:SIZE-1];
  
  // Write
  always @ (posedge clock_i)
  begin
    if (write_i)
    begin
      _content[addr_i+3] <= data_i[31:24];
      _content[addr_i+2] <= data_i[23:16];
      _content[addr_i+1] <= data_i[15:8];
      _content[addr_i  ] <= data_i[7:0];
    end
  end
  
  // Read
  assign data_o = (read_i) ? { _content[addr_i+3], 
                               _content[addr_i+2],
                               _content[addr_i+1],
                               _content[addr_i  ] }
                           : 32'b0;
  
endmodule
